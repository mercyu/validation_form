<?php
//  Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.

header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
      // Если есть параметр save, то выводим сообщение пользователю.
   ?>
    <script>alert('Thank you, the results are saved.');</script>
   <?php 
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  //
  exit();
   
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки./[^a-zа-я ]+/iu
$errors = FALSE;

if (empty($_POST['fio'])) {
  print('Name is required.<br/>');
  $errors = TRUE;
}
else if (!preg_match('/^[A-Za-zа-яА-Я\s]+$/iu',$_POST['fio'])) {
  print('Invalid name format.<br/>');
  $errors = TRUE;
}

if (empty($_POST['email'])) {
    print('Name is required.<br/>');
    $errors = TRUE;
}
else if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
    print('Invalid email format.<br/>');
    $errors = TRUE;
}

$date = $_POST['date'];
if (empty($_POST['date'])) {
    print('Date is required.<br/>');
    $errors = TRUE;
}

if (!$_POST['sex']){
    print('Sex is required.<br/>');
    $errors = TRUE;
}

if (!$_POST['edu']){
    print('Education is required.<br/>');
    $errors = TRUE;
}

if (empty($_POST['abilities'])) {
    print('Course is required.<br/>');
    $errors = TRUE;
}
/* код в этом комментарии не вносит данные в базу
 * $ability_data = array_values($ability_labels);
  else{
   
    foreach ($abilities as $ability) {
        if (in_array($ability, $ability_data)===0) {
            print('Неудачный выбор!<br/>');
            $errors = TRUE;
        }
    }
}
$ability_insert = [];
foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}
*/

$ability1 = in_array('Java', $_POST['abilities']) ? 1 : 0;
$ability2 = in_array('Python', $_POST['abilities']) ? 1 : 0;
$ability3 = in_array('PHP', $_POST['abilities']) ? 1 : 0;
$ability4 = in_array('C', $_POST['abilities']) ? 1 : 0;

if (!$_POST['comment']){
    print('Comment is required.<br/>');
    $errors = TRUE;
}

if (!$_POST['check']){
    print('Consent is required.<br/>');
    $errors = TRUE;
}

// *************

if ($errors) {
    // При наличии ошибок завершаем работу скрипта.
  exit();
}
//Сохраняем в бд

$user = 'u20378';
$pass = '3861391';
$db = new PDO('mysql:host=localhost;dbname=u20378', $user, $pass,
  array(PDO::ATTR_PERSISTENT => true));

//Подготовленный запрос
//неименованные метки 

try {$stmt = $db->prepare("INSERT INTO application SET fio = ?, email = ?, date = ?, sex = ?, education = ?, Java = ?, Python = ?, PHP = ?, C = ?, comment = ?");
$stmt->execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$_POST['sex'],$_POST['edu'],$ability1,$ability2,$ability3,$ability4,$_POST['comment']));
  }  
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}

//  stmt - это "дескриптор состояния".

// именованные 
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));

/*$stmt = $db->prepare("INSERT INTO application (fio, email, date, sex, education, ability_Java, ability_Python, ability_PHP, ability_C, comment) VALUES (:fi0, :email, :date, :sex, :education, :ability_Java, :ability_Python, :ability_PHP, :ability_C, :comment)");
 $stmt->bindParam(':fio', $fio);
 $stmt->bindParam(':email', $email);
 $stmt->bindParam(':date', $date);
 $stmt->bindParam(':sex', $sex);
 $stmt->bindParam(':education', $education);
 $stmt->bindParam(':ability_Java', $ability_insert_Java);
 $stmt->bindParam(':ability_Python', $ability_insert_Python);
 $stmt->bindParam(':ability_PHP', $ability_insert_PHP);
 $stmt->bindParam(':ability_C', $ability_insert_C);
 $stmt->bindParam(':comment', $comment);
 $fio = $_POST['fio'];
 $email = $_POST['email'];
 $date = $_POST['date'];
 $sex = $_POST['sex'];
 $education = $_POST['education'];
 $ability_Java = $ability_1;
 $ability_Python = $ability_2;
 $ability_PHP = $ability_3;
 $ability_C = $ability_4;
 $comment = $_POST['comment'];
 $stmt->execute();
 */
//$ability_labels = ['Java' => 'Java', 'Python' => 'Python', 'PHP' => 'PHP', 'C' => 'C'];

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
